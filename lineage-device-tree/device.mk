#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Enable updating of APEXes
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

# Include GSI keys
$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)

# fastbootd
PRODUCT_PACKAGES += \
    android.hardware.fastboot@1.1-impl-mock \
    fastbootd

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Partitions
PRODUCT_BUILD_SUPER_PARTITION := false
PRODUCT_USE_DYNAMIC_PARTITIONS := true

# Product characteristics
PRODUCT_CHARACTERISTICS := tablet

# Rootdir
PRODUCT_PACKAGES += \
    init.crda.sh \
    init.qti.media.sh \
    init.qcom.sdio.sh \
    init.qcom.sensors.sh \
    init.qti.dcvs.sh \
    init.qcom.efs.sync.sh \
    init.qcom.post_boot.sh \
    init.mdm.sh \
    init.qcom.usb.sh \
    qca6234-service.sh \
    init.qcom.early_boot.sh \
    init.qcom.sh \
    init.qcom.class_core.sh \
    init.qcom.coex.sh \
    init.qti.early_init.sh \
    init.qti.display_boot.sh \
    install-recovery.sh \
    init.qti.chg_policy.sh \
    init.qti.qcv.sh \
    init.class_main.sh \

PRODUCT_PACKAGES += \
    fstab.emmc \
    init.qcom.factory.rc \
    init.qti.ufs.rc \
    init.qcom.rc \
    init.samsung.bsp.rc \
    init.gta4lxx.rc \
    init.qcom.usb.rc \
    init.samsung.rc \
    init.target.rc \
    init.gta4l.rc \
    init.recovery.qcom.rc \

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/rootdir/etc/fstab.emmc:$(TARGET_COPY_OUT_RAMDISK)/fstab.emmc

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 29

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/samsung/gta4l/gta4l-vendor.mk)
